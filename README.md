# Workspace Web Edition Service Client API Sample #

This project implements a custom workspace tab and interaction tab in Genesys Workspace Web Edition using the Service Client API.

The code is meant to accompany the following two articles on the Genesys DevFoundry:

[Customizing Genesys Workspace Web-Edition – Part 1 – An Interaction Tab](http://docs.genesys.com/developer/index.php/2015/09/15/customizing-genesys-workspace-web-edition/)

[Customizing Genesys Workspace Web-Edition – Part 2 – A Workspace Tab](http://docs.genesys.com/developer/index.php/2015/09/29/customizing-genesys-workspace-web-edition-part-2-a-workspace-tab/)